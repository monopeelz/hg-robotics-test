## Assignment

The mission planning system is the software that sends the messages to the drone in order to indicate the flight paths.
In agriculture business, drones fly within the specific area and spray the pesticide. A task contains the following
properties, Total Area, Flight Distance, Pesticide Volume and Speed. Usually, each user prefers different types of
units, e.g. Thai users prefer ‘Rai’ (ไร) for Total Area while the others might prefer ‘Acre’ for the property. Thus,
unit conversion is one of the most important features for the application.

Your task is to implement the unit conversion system as per following requirements:

- Ability to choose a measurement system for displaying (Metric, SI, etc.).
- Ability to override a unit for an individual property (Total Area, Flight Distance, Pesticide Volume, Speed). For
  example, users choose SI system but only display “Total Area” in Rai.
- Ability to add more units (miles, sq.ft. etc.)
- Ability to add more measurement systems (e.g., Thai area measurement system - Rai, Sq. Wa, Ngan)
- Ensure that your code works correctly

## Install

Clone project

```
git clone https://monopeelz@bitbucket.org/monopeelz/hg-robotics-test.git hg-test
```

Prepare backend

```
cd hg-test/backend
pip install -r requirement.txt
python ./manage.py migrate
python ./manage.py loaddata masterdata.json # load master data
python ./manage.py runserver 0.0.0.0:8000 # run server
```

Prepare frontend

```
cd hg-test/frontend
npm i -g yarn
yarn
yarn start
```

Open webbrowser http://localhost:3000
