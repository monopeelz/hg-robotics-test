from django.db import models


class MeasurementSystem(models.Model):
    name = models.CharField(max_length=150)
    is_default = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class UnitType(models.Model):
    name = models.CharField(max_length=150)

    def __str__(self):
        return self.name


class Unit(models.Model):
    name = models.CharField(max_length=150, unique=True)
    ratio = models.FloatField(default=1)
    type = models.ForeignKey(UnitType, on_delete=models.CASCADE)
    system = models.ForeignKey(MeasurementSystem, null=True, on_delete=models.CASCADE)

    unique_together = ('type', 'system',)

    def __str__(self):
        return self.name
