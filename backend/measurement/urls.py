from django.contrib import admin
from django.urls import path, include
from rest_framework import routers

from measurement.views import MeasurementSystemView, UnitView, UnitTypeView

router = routers.DefaultRouter()
router.register(r'systems', MeasurementSystemView)
router.register(r'units', UnitView)
router.register(r'unit-types', UnitTypeView)

urlpatterns = [
    path('', include(router.urls)),
]
