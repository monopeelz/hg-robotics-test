# Create your views here.
from rest_framework import viewsets

from measurement.models import MeasurementSystem, Unit, UnitType
from measurement.serializers import MeasurementSystemSerializer, UnitTypeSerializer, UnitSerializer, \
    UnitDetailSerializer


class MeasurementSystemView(viewsets.ModelViewSet):
    queryset = MeasurementSystem.objects.all()
    serializer_class = MeasurementSystemSerializer


class UnitView(viewsets.ModelViewSet):
    queryset = Unit.objects.all()
    serializer_class = UnitSerializer

    def get_serializer_class(self):
        if self.action in ['list', 'retrieve']:
            return UnitDetailSerializer
        return self.serializer_class


class UnitTypeView(viewsets.ModelViewSet):
    queryset = UnitType.objects.all()
    serializer_class = UnitTypeSerializer
