# Create your tests here.
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from measurement.models import MeasurementSystem, Unit, UnitType


class UnitTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = UnitType
        fields = [
            'id',
            'name',
        ]


class UnitSerializer(serializers.ModelSerializer):
    class Meta:
        model = Unit
        fields = [
            'id',
            'ratio',
            'name',
            'type',
            'system'
        ]

    # def validate(self, attrs):
    #     if attrs['system'] is not None and Unit.objects.filter(system=attrs['system'], type=attrs['system']):
    #         system = MeasurementSystem.objects.get(pk=attrs['system'])
    #         unit_type = UnitType.objects.get(pk=attrs['type'])
    #         raise ValidationError(f"system {system} already have type {unit_type} setting")
    #     return attrs


class MeasurementSystemSerializer(serializers.ModelSerializer):
    class UnitTypeSerializer(serializers.ModelSerializer):
        class Meta:
            model = UnitType
            fields = [
                'id',
                'name',
            ]

    class UnitSerializer(serializers.ModelSerializer):
        type = UnitTypeSerializer(serializers.ModelSerializer)

        class Meta:
            model = Unit
            fields = [
                'id',
                'name',
                'type',
            ]

    units = UnitSerializer(many=True, read_only=True)

    class Meta:
        model = MeasurementSystem
        fields = [
            'id',
            'name',
            'is_default',
            'units'
        ]


class UnitDetailSerializer(serializers.ModelSerializer):
    class MeasurementSystemSerializer(serializers.ModelSerializer):
        class Meta:
            model = MeasurementSystem
            fields = [
                'id',
                'name',
                'is_default'
            ]

    type = UnitTypeSerializer(read_only=True)
    system = MeasurementSystemSerializer(read_only=True)

    class Meta:
        model = Unit
        fields = [
            'id',
            'name',
            'ratio',
            'type',
            'system'
        ]
