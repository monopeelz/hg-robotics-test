import {useState} from "react";
import {Link, Redirect, Route, Switch, useLocation} from "react-router-dom";
import {Layout, Menu} from 'antd';
import {Content} from "antd/es/layout/layout";
import Sider from "antd/es/layout/Sider";
import Convertor from "./pages/Convertor";
import {MeasurementSystem} from "./pages/MeasurementSystem";
import Home from "./pages/Home";
import {Unit} from "./pages/Unit";
import {UnitType} from "./pages/UnitType";


function App(props: any) {

    const [current, setCurrent] = useState("1")
    const location = useLocation();

    const handleClick = (e: any) => {
        setCurrent(e.key)
    };
    console.log(location)
    return (
            <Layout>
                <Sider width={200} className="site-layout-background">
                    <Menu
                        mode="inline"
                        defaultSelectedKeys={[location.pathname]}
                        defaultOpenKeys={[`/${location.pathname.split('/')[1]}`]}
                        style={{height: '100%', borderRight: 0}}
                    >
                        <Menu.Item key="/home">
                            <Link to={"/home"}>Home</Link>
                        </Menu.Item>
                        <Menu.Item key="/convertor">
                            <Link to={"/convertor"}>Convertor</Link>
                        </Menu.Item>
                        <Menu.Item key="/system">
                            <Link to={"/system"}>Measurement System</Link>
                        </Menu.Item>
                        <Menu.Item key="/unit">
                            <Link to={"/unit"}>Unit</Link>
                        </Menu.Item>
                        <Menu.Item key="/unit-type">
                            <Link to={"/unit-type"}>Unit Type</Link>
                        </Menu.Item>
                    </Menu>
                </Sider>
                <Layout>
                    <Content
                        className="site-layout-background"
                        style={{
                            padding: 24,
                            margin: 0,
                            minHeight: 280,
                        }}
                    >
                        <Switch>
                            <Route exact path="/">
                                <Redirect to="/home" />
                            </Route>
                            <Route path="/home">
                                <Home/>
                            </Route>
                            <Route path="/convertor">
                                <Convertor/>
                            </Route>
                            <Route path="/system">
                                <MeasurementSystem/>
                            </Route>
                            <Route path="/unit">
                                <Unit/>
                            </Route>
                            <Route path="/unit-type">
                                <UnitType/>
                            </Route>
                        </Switch>
                    </Content>
                </Layout>
            </Layout>
    );
}

export default App;
