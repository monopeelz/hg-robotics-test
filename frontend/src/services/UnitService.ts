import axios from "axios"

const baseURL = process.env.REACT_APP_MEASUREMENT_API_URL

export async function getUnit() {
    const res = await axios.get(`${baseURL}/api/units/`)
    return res.data
}

export async function deleteUnit(id: number) {
    const res = await axios.delete(`${baseURL}/api/units/${id}/`)
    return res.data
}

export async function createUnit(data: any) {
    const res = await axios.post(`${baseURL}/api/units/`, data)
    return res.data
}

export async function updateUnit(id: number | undefined, data: any) {
    const res = await axios.put(`${baseURL}/api/units/${id}/`, data)
    return res.data
}
