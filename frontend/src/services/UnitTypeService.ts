import axios from "axios"

const baseURL = process.env.REACT_APP_MEASUREMENT_API_URL

export async function getUnitType() {
    const res = await axios.get(`${baseURL}/api/unit-types/`)
    return res.data
}

export async function deleteUnitType(id: number) {
    const res = await axios.delete(`${baseURL}/api/unit-types/${id}/`)
    return res.data
}

export async function createUnitType(data: any) {
    const res = await axios.post(`${baseURL}/api/unit-types/`, data)
    return res.data
}

export async function updateUnitType(id: number) {
    const res = await axios.put(`${baseURL}/api/unit-types/${id}/`)
    return res.data
}
