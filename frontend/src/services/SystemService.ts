import axios from "axios"

const baseURL = process.env.REACT_APP_MEASUREMENT_API_URL

export async function getSystem() {
    const res = await axios.get(`${baseURL}/api/systems/`)
    return res.data
}

export async function deleteSystem(id: number) {
    const res = await axios.delete(`${baseURL}/api/systems/${id}/`)
    return res.data
}

export async function createSystem(data: any) {
    const res = await axios.post(`${baseURL}/api/systems/`, data)
    return res.data
}

export async function updateSystem(id: number) {
    const res = await axios.put(`${baseURL}/api/systems/${id}/`)
    return res.data
}
