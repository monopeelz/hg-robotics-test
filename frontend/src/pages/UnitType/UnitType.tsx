import React from "react";
import {Button, Col, Form, Input, Layout, Modal, notification, Row, Table} from "antd";
import axios from "axios";
import {createUnitType, getUnitType} from "../../services/UnitTypeService";

export const UnitType: React.FC = () => {

    const [systems, setSystems] = React.useState([])
    const [openForm, setOpenForm] = React.useState(false)

    const [createForm] = Form.useForm()

    const initial = async () => {
        setSystems(await getUnitType())
    }

    const handleOk = async () => {
        const name = createForm.getFieldValue(['name'])
        try {
            await createUnitType({name: name})
            await getUnitType()
            notification.success({message: "Success"})
        } catch (err: any) {
            if (axios.isAxiosError(err)) {
                notification.error({
                    message: "Error",
                    description: JSON.stringify(err.response?.data)
                })
            } else {
                notification.error({message: String(err)})
            }
        } finally {
            setOpenForm(false)
        }
    }

    const handleCancel = () => {
        setOpenForm(false)
    }
    React.useEffect(() => {
        initial()
    }, [])

    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
        }
    ];
    return <Layout>
        <Row gutter={8}>
            <Col>
                <Button onClick={() => setOpenForm(true)}>Create</Button>
            </Col>
        </Row>
        <Row>
            <Col>
                <Table
                    columns={columns}
                    dataSource={systems}
                >
                </Table>
            </Col>
        </Row>
        <Modal
            title="Create"
            visible={openForm}
            onOk={handleOk}
            onCancel={handleCancel}
        >
            <Form form={createForm}>
                <Form.Item label={"name"}
                           name={'name'}
                >
                    <Input required/>
                </Form.Item>
            </Form>
        </Modal>
    </Layout>
}
