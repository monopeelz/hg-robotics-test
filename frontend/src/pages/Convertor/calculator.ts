import {Unit} from "./interfaces";


interface CalculatorProps {
    defaultUnit: Unit
    source_value: number
    source_unit: Unit
    target_unit: Unit
}


export function calculate(props: CalculatorProps): number {
    const {defaultUnit, source_value, source_unit, target_unit} = props
    let defaultValue = 0
    if (source_unit !== defaultUnit) {
        defaultValue = source_value * source_unit.ratio
    } else {
        defaultValue = source_value
    }
    return defaultValue / target_unit.ratio
}
