export interface System {
    id: number
    name: string
    is_default: boolean
}

export interface UnitType {
    id: number
    name: string
}

export interface Unit {
    id: number
    name: string
    type: UnitType,
    system: System
    ratio: number
}

