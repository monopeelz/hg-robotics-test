import React, {useEffect} from "react";
import {Button, Col, Form, Row, Select} from "antd";
import InputSource from "../../components/InputSource";
import DisplayConverResult from "../../components/DisplayConverResult";
import {getSystem} from "../../services/SystemService";
import {Unit, UnitType} from "./interfaces";
import {getUnitType} from "../../services/UnitTypeService";
import {calculate} from "./calculator";
import {getUnit} from "../../services/UnitService";

function capitalize(word: string) {
    return word[0].toUpperCase() + word.slice(1).toLowerCase();
}

export const Convertor: React.FC = () => {

    const [units, setUnits] = React.useState<Array<Unit>>([])
    const [systems, setSystems] = React.useState([])
    const [unitTypes, setUnitTypes] = React.useState<Array<UnitType>>([])

    const [form] = Form.useForm();

    useEffect(() => {
        (async () => {
            setUnits(await getUnit())
            setUnitTypes(await getUnitType())
            setSystems(await getSystem())
        })()
    }, [])

    function onChangeSourceSystem(value: any) {
        const fields: any = {}
        for (const t of unitTypes) {
            const k = 'source' + capitalize(t.name)
            fields[k] = {
                unit: units.filter((u: any) => u.system?.id === value && u.type?.name === t.name)[0]?.id
            }
        }
        form.setFieldsValue({...fields})
    }


    function onChangeConvertSystem(value: any) {
        const fields: any = {}
        for (const t of unitTypes) {
            const k = 'convert' + capitalize(t.name)
            fields[k] = {
                unit: units.filter((u: any) => u.system?.id === value && u.type?.name === t.name)[0]?.id
            }
        }
        form.setFieldsValue({...fields})
    }

    function getDefaultUnit(type: number): Unit {
        return units.filter((u: Unit) => u.system?.is_default && u.type?.id === type)[0]
    }

    function getLocalUnit(id: number): Unit {
        return units.filter((u: Unit) => u.id === id)[0]
    }

    function handleOnFinish() {
        const fields: any = {}
        for (const t of unitTypes) {
            const capKey = capitalize(t.name)
            const source_unit_id = form.getFieldValue(['source' + capKey, 'unit'])
            const source_value = form.getFieldValue(['source' + capKey, 'value'])
            const target_unit_id = form.getFieldValue(['convert' + capKey, 'unit'])

            fields['convert' + capKey] = {
                value: calculate({
                    defaultUnit: getDefaultUnit(t.id),
                    source_unit: getLocalUnit(source_unit_id),
                    source_value: source_value,
                    target_unit: getLocalUnit(target_unit_id),
                })
            }
        }
        form.setFieldsValue({...fields})
    }

    return (
        <Form
            key={"convertor"}
            form={form}
            labelCol={{span: 8}}
            wrapperCol={{span: 24}}
            layout="horizontal"
            onFinish={handleOnFinish}
        >
            <Row gutter={8}>

                <Col span={8}>
                    <Form.Item label={"System (optional)"}>
                        <Select style={{width: '50%'}}
                                onChange={(value: any) => onChangeSourceSystem(value)}
                                placeholder={"Select Measurement System (Optional)"}
                        >
                            {systems.map((s: any) => <Select.Option value={s.id}>{s.name}</Select.Option>)}
                        </Select>
                    </Form.Item>
                    {unitTypes.map((t: UnitType) => <React.Fragment key={t.id}><InputSource
                        name={"source" + capitalize(t.name)}
                        source={units.filter((u: any) => u.type.name === t.name)}
                        label={t.name.charAt(0).toUpperCase() + t.name.slice(1)}
                    /></React.Fragment>)}
                </Col>
                <Col
                    style={{alignContent: 'center'}}
                    span={2}
                >
                    <Button htmlType="submit" type={"primary"}>Convert</Button>

                </Col>
                <Col
                    span={8}
                >
                    <Form.Item label={"System (optional)"}>
                        <Select style={{width: '50%'}}
                                onChange={onChangeConvertSystem}
                                placeholder={"Select Measurement System (Optional)"}
                        >
                            {systems.map((s: any) => <Select.Option value={s.id}>{s.name}</Select.Option>)}
                        </Select>
                    </Form.Item>
                    {unitTypes.map((t: UnitType) => <React.Fragment key={`convert-${t.id}`}><DisplayConverResult
                        name={"convert" + capitalize(t.name)}
                        source={units.filter((u: any) => u.type.name === t.name)}
                        label={t.name.charAt(0).toUpperCase() + t.name.slice(1)}
                    /></React.Fragment>)}

                </Col>
            </Row>
        </Form>
    )
}
