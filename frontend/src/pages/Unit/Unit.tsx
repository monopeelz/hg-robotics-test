import React from "react";
import {Button, Col, Form, Input, InputNumber, Layout, Modal, notification, Row, Select, Table} from "antd";
import axios from "axios";
import {createUnit, getUnit, updateUnit} from "../../services/UnitService";
import {getSystem} from "../../services/SystemService";
import {getUnitType} from "../../services/UnitTypeService";
import {System, Unit as IUnit} from "../Convertor/interfaces";

export const Unit: React.FC = () => {

    const [units, setUnits] = React.useState([])
    const [systems, setSystems] = React.useState([])
    const [unitTypes, setUnitTypes] = React.useState([])
    const [selectItem, setSelectItem] = React.useState<IUnit | null>(null)

    const [openForm, setOpenForm] = React.useState(false)
    const [openUpdateForm, setOpenUpdateForm] = React.useState(false)


    const [createForm] = Form.useForm()
    const [updateForm] = Form.useForm()

    const initial = async () => {
        setUnits(await getUnit())
        setSystems(await getSystem())
        setUnitTypes(await getUnitType())
    }

    const handleCreateForm = async () => {
        try {
            await createUnit({
                name: createForm.getFieldValue(['name']),
                ratio: createForm.getFieldValue(['ratio']),
                system: createForm.getFieldValue(['system_id']),
                type: createForm.getFieldValue(['type_id']),
            })
            setUnits(await getUnit())
            notification.success({message: "Success"})
        } catch (err: any) {
            if (axios.isAxiosError(err)) {
                notification.error({
                    message: "Error",
                    description: JSON.stringify(err.response?.data)
                })
            } else {
                notification.error({message: String(err)})
            }
        } finally {
            setOpenForm(false)
        }
    }

    const handleUpdateForm = async () => {
        try {
            await updateUnit(selectItem?.id, {
                name: updateForm.getFieldValue(['name']),
                ratio: updateForm.getFieldValue(['ratio']),
                system: updateForm.getFieldValue(['system_id']),
                type: updateForm.getFieldValue(['type_id']),
            })
            setUnits(await getUnit())
            notification.success({message: "Success"})
        } catch (err: any) {
            if (axios.isAxiosError(err)) {
                notification.error({
                    message: "Error",
                    description: JSON.stringify(err.response?.data)
                })
            } else {
                notification.error({message: String(err)})
            }
        } finally {
            setOpenUpdateForm(false)
        }
    }

    const handleCancel = () => {
        setOpenForm(false)
    }

    const handleSelect = (u: IUnit) => {
        setSelectItem(u)
        setOpenUpdateForm(true)
        updateForm.setFieldsValue({
            name: u.name,
            ratio: u.ratio,
            type_id: u.type?.id,
            system_id: u.system?.id,
        })
    }

    React.useEffect(() => {
        initial()
    }, [])

    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            render: (text: any, record: any) => <a onClick={() => handleSelect(record)}>{text}</a>
        },
        {
            title: 'Ratio',
            dataIndex: 'ratio',
        },
        {
            title: 'Type',
            dataIndex: 'type',
            render: (text: any, record: any) => record.type.name
        },
        {
            title: 'System',
            dataIndex: 'system',
            render: (text: any, record: any) => record.system?.name
        }
    ];
    return <Layout>
        <Row>
            <Col>
                <Button onClick={() => setOpenForm(true)}>Create</Button>
            </Col>
        </Row>
        <Row>
            <Col>
                <Table
                    columns={columns}
                    dataSource={units}
                >
                </Table>
            </Col>
        </Row>
        <Modal
            title="Create"
            visible={openForm}
            onOk={handleCreateForm}
            onCancel={handleCancel}
        >
            <Form form={createForm}
                  labelCol={{span: 4}}
                  wrapperCol={{span: 12}}
                  layout="horizontal"
            >
                <Form.Item label={"Name"}
                           name={'name'}
                >
                    <Input required/>
                </Form.Item>
                <Form.Item label={"Ratio"}
                           name={'ratio'}
                >
                    <InputNumber required/>
                </Form.Item>
                <Form.Item label={"System"}
                           name={'system_id'}
                >
                    <Select
                        placeholder="Select System"
                    >
                        <Select.Option value={""}>{"-----"}</Select.Option>
                        {systems.map((s: System) => <Select.Option value={s.id}>{s.name}</Select.Option>)}
                    </Select>
                </Form.Item>
                <Form.Item label={"Type"}
                           name={'type_id'}
                >
                    <Select
                        placeholder="Select Type"
                    >
                        {unitTypes.map((s: System) => <Select.Option value={s.id}>{s.name}</Select.Option>)}
                    </Select>
                </Form.Item>
            </Form>
        </Modal>

        <Modal
            title="Update"
            visible={openUpdateForm}
            onOk={handleUpdateForm}
            onCancel={() => setOpenUpdateForm(false)}
        >
            <Form form={updateForm}
                  labelCol={{span: 4}}
                  wrapperCol={{span: 12}}
                  layout="horizontal"
            >
                <Form.Item
                    label={"Name"}
                    name={'name'}
                >
                    <Input value={selectItem?.name} required/>
                </Form.Item>
                <Form.Item label={"Ratio"}
                           name={'ratio'}
                >
                    <InputNumber value={selectItem?.ratio} required/>
                </Form.Item>
                <Form.Item label={"System"}
                           name={'system_id'}
                >
                    <Select
                        value={selectItem?.system?.id}
                        placeholder="Select System"
                    >
                        <Select.Option value={""}>{"-----"}</Select.Option>
                        {systems.map((s: System) => <Select.Option value={s.id}>{s.name}</Select.Option>)}
                    </Select>
                </Form.Item>
                <Form.Item label={"Type"}
                           name={'type_id'}
                >
                    <Select
                        value={selectItem?.type.id}
                        placeholder="Select Type"
                    >
                        {unitTypes.map((s: System) => <Select.Option value={s.id}>{s.name}</Select.Option>)}
                    </Select>
                </Form.Item>
            </Form>
        </Modal>
    </Layout>
}
