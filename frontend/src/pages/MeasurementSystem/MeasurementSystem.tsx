import React from "react";
import {createSystem, getSystem} from "../../services/SystemService";
import {Button, Col, Form, Input, Layout, Modal, notification, Row, Table} from "antd";
import axios from "axios";

export const MeasurementSystem: React.FC = () => {

    const [systems, setSystems] = React.useState([])
    const [openForm, setOpenForm] = React.useState(false)

    const [createForm] = Form.useForm()

    const initial = async () => {
        setSystems(await getSystem())
    }

    const handleOk = async () => {
        const name = createForm.getFieldValue(['name'])
        try {
            await createSystem({name: name})
            notification.success({message: "Success"})
        } catch (err: any) {
            if (axios.isAxiosError(err)) {
                notification.error({
                    message: "Error",
                    description: JSON.stringify(err.response?.data)
                })
            } else {
                notification.error({message: String(err)})
            }
        } finally {
            setOpenForm(false)
            await getSystem()
        }
    }

    const handleCancel = () => {
        setOpenForm(false)
    }

    React.useEffect(() => {
        initial()
    }, [])

    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
        },
        {
            title: 'Default',
            dataIndex: 'is_default',
            render: (text: boolean) => text ? "Yes" : "No"
        }
    ];
    return <Layout>
        <Row>
            <Col>
                <Button onClick={() => setOpenForm(true)}>Create</Button>
            </Col>
        </Row>
        <Row>
            <Col>
                <Table
                    columns={columns}
                    dataSource={systems}
                >
                </Table>
            </Col>
        </Row>
        <Modal
            title="Create"
            visible={openForm}
            onOk={handleOk}
            onCancel={handleCancel}
        >
            <Form form={createForm}>
                <Form.Item label={"name"}
                           name={'name'}
                >
                    <Input required/>
                </Form.Item>
            </Form>
        </Modal>
    </Layout>
}
