import React from "react";
import {Form, Input, Select} from "antd";


interface DisplayResultProps {
    ref?: any
    label: string
    name: string
    unitType?: string
    source: any
    systemSelect?: any
}


const DisplayCovertResult: React.FC<DisplayResultProps> = ({ref, label, name, source, systemSelect}) => {
    return <Form.Item label={label}>
        <Input.Group compact>
            <Form.Item
                name={[name, 'value']}
                noStyle
            >
                <Input style={{width: '50%'}} placeholder="Display Value" disabled/>
            </Form.Item>
            <Form.Item
                name={[name, 'unit']}
                noStyle
                rules={[{required: true, message: 'Select UnitType'}]}
            >
                <Select placeholder="Select UnitType"
                        style={{width: '40%'}}
                >
                    {source.map((u: any) => <Select.Option
                        key={`convert-${label}-${u.id}`}
                        value={u.id}
                    >{u.name} ({u.system?.name})</Select.Option>)}
                </Select>
            </Form.Item>
        </Input.Group>
    </Form.Item>

}
export default DisplayCovertResult
