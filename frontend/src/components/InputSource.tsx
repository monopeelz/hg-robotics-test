import React from "react";
import {Form, Input, InputNumber, Select} from "antd";

interface InputDetailProps {
    label: string
    name: string
    unitType?: string
    source: any
}


const InputSource: React.FC<InputDetailProps> = ({label, name, source}) => {
    return <Form.Item label={label}>
        <Input.Group compact>
            <Form.Item
                name={[name, 'value']}
                noStyle
                rules={[{required: true, message: 'Value is required'}]}
            >
                <InputNumber style={{width: '50%'}} placeholder="Input Value"/>
            </Form.Item>
            <Form.Item
                name={[name, 'unit']}
                noStyle
                rules={[{required: true, message: 'Select UnitType'}]}
            >
                <Select
                    style={{width: '40%'}}
                    placeholder="Select UnitType"
                >
                    {source.map((u: any) => <Select.Option key={`${u.id}`}
                                                           value={u.id}>{u.name} ({u.system?.name})</Select.Option>)}
                </Select>
            </Form.Item>
        </Input.Group>
    </Form.Item>

}
export default InputSource
